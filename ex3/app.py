from flask import Flask, jsonify
from flask_restful import Api
import biofunctions

app = Flask(__name__)
api = Api(app)

@app.route('/api/genbank/<accession>', methods=['GET'])
def get_genbank(accession):
    info = biofunctions.get_genbank_info(accession)
    return jsonify(info) 

if __name__ == "__main__":
    app.run(debug=True) 


