from Bio import SeqIO
from Bio.Seq import Seq
import os

FASTA_DIR = os.path.join(os.path.dirname(__file__), 'data')
GENBANK_DIR = os.path.join(os.path.dirname(__file__), 'data')


def get_genbank_info(accession):
    genbank_file = os.path.join(GENBANK_DIR, f'{accession}.genbank')  
    print(genbank_file)
    try:
        records = SeqIO.parse(genbank_file, 'genbank')
        info = []

        for record in records:
            # Extracting information
            accession_id = record.id
            title = record.description
            organism = record.annotations.get('organism', 'N/A')
            ncbi_link = f'https://www.ncbi.nlm.nih.gov/nuccore/{accession_id}'
            latest_reference = record.annotations.get('references', [])[0].title if record.annotations.get('references') else 'N/A'
            num_features = len(record.features)
            cds_info = [{'type': feature.type, 'location': str(feature.location)} for feature in record.features if feature.type == 'CDS']
            sequence = str(record.seq)
            first_90_chars = sequence[:90]
            

            translation = str(Seq(sequence).translate())
            first_30_aa = translation[:30]

        
            record_info = {
                'title': title,
                'accession': accession_id,
                'organism': organism,
                'ncbi_link': ncbi_link,
                'latest_reference': latest_reference,
                'num_features': num_features,
                'cds_info': cds_info,
                'sequence_first_90_chars': first_90_chars,
                'translation_first_30_aa': first_30_aa,
            }
            info.append(record_info)

        return info

    except FileNotFoundError:
        return {'error': f'No se encuentra ningún archivo genbank con la accession {accession}'}

""" result = get_genbank_info("AY156734")
print(result) """
