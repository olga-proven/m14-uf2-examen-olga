from Bio import Entrez, SeqIO
import os

Entrez.email = "olgo3000@office.proven.cat"

organism = "Plasmodium falciparum"
gen = "CRT"

def search_accession_id(gen, organism):
    query = f'{organism}[Organism] AND {gen}[Gene]'
    handle = Entrez.esearch(db='nucleotide', term=query, retmax=30)
    result = Entrez.read(handle)
    return result['IdList']

def download_genbank_fasta(accession_ids):
    data_folder = "data"
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    for accession_id in accession_ids:
        # Check if the file already exists locally
        filename_genbank = f'{gen}_{accession_id}.gb'
        filename_fasta = f'{gen}_{accession_id}.fasta'

        if os.path.isfile(os.path.join(data_folder, filename_genbank)) and os.path.isfile(os.path.join(data_folder, filename_fasta)):
            print(f"GenBank and FASTA files for {gen}_{accession_id} already exist.")
        else:
            # Download GenBank file using the Accession ID
            genbank_handle = Entrez.efetch(db='nucleotide', id=accession_id, rettype='gb', retmode='text')
            genbank_data = genbank_handle.read()

            # Save the GenBank file in the "data" folder
            genbank_filepath = os.path.join(data_folder, filename_genbank)
            with open(genbank_filepath, 'w') as file:
                file.write(genbank_data)
            print(f"GenBank file saved for {gen}_{accession_id}: {genbank_filepath}")

            # Download FASTA file using the Accession ID
            fasta_handle = Entrez.efetch(db='nucleotide', id=accession_id, rettype='fasta', retmode='text')
            fasta_data = fasta_handle.read()

            # Save the FASTA file in the "data" folder
            fasta_filepath = os.path.join(data_folder, filename_fasta)
            with open(fasta_filepath, 'w') as file:
                file.write(fasta_data)
            print(f"FASTA file saved for {gen}_{accession_id}: {fasta_filepath}")

# Search for Accession IDs
accession_ids = search_accession_id(gen, organism)

# Download GenBank and FASTA files
download_genbank_fasta(accession_ids)
