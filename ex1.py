#import requests
import os

# Names of DNA molecules and their respective Wikipedia URLs
molecules = ["adenine","thymine","guanine","cytosine"]


# Download and display information for each molecule
for molecule in molecules:
    print(f"\nMolecule: {molecule}")

    # PubChem URL
    pubchem_link = f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/{molecule}/SDF"
    print("PubChem URL:", pubchem_link)

    # Wikipedia URL
    wiki_link= f"https://en.wikipedia.org/wiki/{molecule}"
    print("Wikipedia:", wiki_link)

    # Check if the file already exists on disk
    file_path = f"{molecule}.sdf"
    if os.path.exists(file_path):
        print("The file already exists on disk.")
    