import sys
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.SeqUtils import GC

file = "M14-UF2-Examen/data/ls_orchid.fasta"
if len(sys.argv) != 2:
    print("Usage: " + sys.argv[0] + " <FASTA file of DNA or RNA>")
    print(f"Since no file provided, using default: {file}")
else:
    file = sys.argv[1]

# Read file
try:
    sequences = SeqIO.parse(open(file), "fasta")
    print(f'Reading file {file}')
except Exception as e:
    print(f"Unable to read the file: {e}")
    sys.exit(1)

# Initialize a list to store SeqRecords of proteins
seq_records_protein = []

# Iterate through each seq_record and perform the required operations
for seq_record in sequences:
    # 1. Display the header
    print("Header:", seq_record.id)

    # 2. Display the DNA sequence
    print("DNA sequence:", seq_record.seq)

    # 3. Transcribe to RNA
    rna = seq_record.seq.transcribe()
    print("Transcription to RNA:", rna)

     # 4. Total length of the sequence
    length_total = len(seq_record)
    print("Total length of the sequence:", length_total)

    # 5. GC percentage
    #gc_percentage = SeqIO.GC(seq_record.seq)
    #print("GC Percentage:", gc_percentage)

    # Calcular el porcentaje GC
    porcentaje_gc = (GC(seq_record.seq) / length_total) * 100
    print("GC Percentage:", porcentaje_gc)

    # 6. Ambiguous DNA or not
    if set(seq_record.seq) - set('ACGT'):
        print("The sequence contains ambiguous bases.")
    else:
        print("The sequence does not contain ambiguous bases.")

    # 7. Translate to protein
    protein = seq_record.seq.translate()
    protein_record = SeqRecord(protein, id=f"{seq_record.id}_protein")
    seq_records_protein.append(protein_record)


